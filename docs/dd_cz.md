# Jak jsem se stal vývojářem Debianu

Toto je příběh z dávných časů, z časů mýtů a legend na přelomu milénia, kdy IT
korporace byly malicherné a kruté a stíhaly lidstvo útrapami. V těch dobách
Microsoft ovládl železnou rukou trh s operačními systémy a s ním i trh s
prohlížeči internetu, čímž uvrhl lidstvo do technologické temnoty. Stroje, které
dříve řídili schopní inženýři v otevřené spolupráci, začali ovládat chamtiví
podnikatelé. Revoluční technlogie, které měly sloužit všem, byly pomalu ale
jistě degradovány na nástroj kontroly bohatých a mocných, kteří chtěli být ještě
bohatší a mocnější.


## Počátky hnutí za otevřený software

Ne všichni však byli ochotní se poklonit mnohohlavé korporátní hydře a moci
jejího téměř neomezeného kapitálu. Z davu se zrodili první hrdinové nové éry -
bojovníci za svobodný a otevřený software.

V roce 1983 založil Richard Stallman projekt GNU ve snaze napsat kompletní
operační systém s otevřeným zdrojovým kódem. V roce 1985 Stallman publikoval GNU
Manifest, který vyzývá ke spolupráci na GNU projektu a vysvětluje důležitost
otevřeného software. Později v roce 1985 Stallman založil neziskovou organizaci _Free
Software Foundation_ (**FSF**, Nadace pro svobodný software), která dodnes bojuje za
svobodný software a práva uživatelů. V roce 1989 Stallman publikoval první verzi
_GNU General Public License_ (**GPL**), svobodné licence, která změnila svět software
navždy.

Ačkoliv mnoho GNU projektů slaví dodnes velký úspěch po celém světě, GNU Hurd
jádro operačního systému mezi ně nepatří. V roce 1991 vypustil Linus Torvalds
zdrojové kódy nového jádra operačního systému jménem Linux. Původní licence
nebyla svobodná, ale Torvalds přelicencoval Linux 0.12 v únoru 1992 na GNU GPL a
projekt přitáhl pozornost mnoha nadšenců z komunity.

V roce 1993 založil Ian Murdock Debian GNU/Linux projekt, který měl za cíl
stvořit kompletně svobodný a otevřený operační systém na Linuxovém jádře a s
plnou dostupností GNU a dalšího svobodného software. Projekt byl od začátku
silně spřízněn s myšlenkami svobodného software jak je definoval GNU projekt a
_Free Software Foundation_.


## Piráti nového milénia

Počítače přilákaly můj zájem hlavně díky pozoruhodným virtuálním světům
videoher. Kolem roku 2004 jsem měl na střední škole nahráno nespočet hodin a můj
koníček mě donutil se velmi dobře seznámit s PC hardwarem i softwarem. Windows
(lidově též widle) jsem přeinstalovával několikrát do roka, jelikož systém neunesl
obrovské množství software, kterým jsem ho pravidelně zásoboval.

Internet byl ještě v plenkách, takže se tehdy pirátilo zejména pomocí CD. Místo
sběratelských karet se vyměňovala fixem popsaná cédéčka plná "nelegálně
distribuovaného" software, instalačních klíčů, cracků, keygenů, patchů, filmů,
seriálů, hudby, knih a všecho možného.

Tehdejší pirátská scéna obsahovala legendární warez skupiny jako SKIDROW,
FAiRLiGHT, DEViANCE či Razor 1911, které dokázaly vycrackovat libovolné systémy
ochrany proti kopírování zcela bez zdrojových kódů. Osobně za ikonu té pirátské
éry považuji Windows XP Pirated Edition, tedy pirátskou verzi Windows XP, která
při startu zobrazovala pirátskou vlajku a fungovala lépe než originální systém.

Korporátní zlo se samozřejmě ze všech sil snažilo potlačit základní lidskou
potřebu sdílet s ostatními ve jménu větších zisků, ale digitální piráti byli
vždy o krok napřed. Masivní kampaň strachu proti sdílení založená na výhružkách
obrovskými pokutami a vězením však mnoho lidí naštvala a motivovala k boji
s korporátní hydrou na jejím vlastním teritoriu, tedy v rámci oficiálních
legálních systémů.


## První ochutnávka Linuxu

Jednoho dne té pirátské éry v náhodném IRC kanále na internetu 1.0 jsem nadával
na rozbité widle a nějaký náhodný člověk mi napsal ať zkusím Debian Linux,
svobodný operační systém. Znělo to zajímavě! Podivné růžové stránky nabídly
stažení ISO obrazu Debian 3.0 Woody, který jsem vypálil na CD a nabootoval.

Instalátor byl strohý v textovém módu, podobný víc MS-DOS než grafickým Windows,
ale vlastně stačilo jen číst, odpovídat na otázky a mačkat ENTER. Zásadní změna
oproti nesvobodnému software, který jsem znal, byla naprostá absence ochrany
proti kopírování. Žádné opisování licenčního klíče, žádné absurdní procesy
aktivace, žádné potvrzování EULA v záměrně kryptickém legálním jazyce. Jen
software.

Instalace se zdařila a po restartu stroje jsem se objevil v úplně novém světě.
Mnohokrát jsem z něj v panice utekl, protože tam chyběl software, na který jsem
byl důvěrně zvyklý. Ale vždy jsem se do toho podivného světa nakonec vrátil dál
objevovat a bojovat, protože tam místo toho byl software jiný - svobodný a
otevřený všem.


## Dvacet tisíc balíků

Můj oblíbený software na Debianu sice nefungoval, ale zato jsem objevil zásadní
nástroj. Správce balíků `apt` dokáže jedním příkazem nainstalovat všechen
software, který je zabalený v repozitářích Debianu! Žádné hledání správného
download tlačítka, žádné klikání na "Další", žádné licenční klíče, cracky, viry,
patche, EULA, a všechny ty obvyklé nesmysly, jen `apt-get install` a je to.
Co víc, stejně jednoduše se lze software také zbavit a nenechává po sobě
bordel v registrech ani nic podobného. Systém opakovanou instalací software
nedegraduje, není ho třeba přeinstalovávat.

Balíky, to je něco revolučního! A v Debianu jich už tehdy bylo kolem 20 000,
pokud si dobře vzpomínám. Byla to právě ta neskutečně pohodlná a efektivní
distribuce software, kterou si mě Debian získal.

V průběhu času jsem důkladně prozkoumal také další linuxové distribuce, zejména
ty s alternativní správou balíku - Gentoo (`portage`), Arch (`pacman`), Fedoru
(`rpm`) a jejich všemožné klony. Každé distro má své výhody a nevýhody, ale
nakonec jsem se vždy vrátil k Debianu pro jeho konzistentně kvalitní balíky,
které jsou adekvátně integrované do systému. Procesy Debianu zaručují velkou
míru stability, což je kritické pro produkční systémy, kde není přípustné aby se
systém nahodně rozbil při aktualizaci.


## Živit se svobodným software

V roce 2009 už ze mě byl nadšený bojovník za svobodný software a každodenní
uživatel Linuxu. Nadšenců přibývalo po celém světě, ale široká veřejnost stále
pojem moc nechápala. Korporátní výplach se snažil svobodné sdílení vyložit jako
nelegální aktivitu a lidem prostě přišlo divné, že by někdo pracoval
zadarmo. V tom musí být nějaký háček.

Když jsem jednoho dne z okna tramvaje viděl logo firmy LinuxBox, přišel jsem se
tam zvědavě podívat, zda bych mohl pracovat na Linuxu a přesně tak to bylo.
Celá firma jela na otevřeném software, který jsem používal k práci a také jsem
ho v rámci práce tvořil, modifikoval a spravoval. V té době jsem poprvé úspěšně
přispěl do otevřeného projektu - jednalo se o vylepšení IAX disektoru pro
Wireshark. Měl jsem také to potěšení psát C modul pro PostgreSQL,
experimentovat se síťovými moduly linuxového jádra, či provětrat vektorovou
algebru s NumPy/SciPy. Bylo zřejmé, že otevřený software nejen funguje, ale
často funguje mnohem lépe než software uzavřený.

Narazil jsem také na problém distribuce specializovaných interních C knihoven, které
jsem napsal jako součást většího systému. Jelikož tehdy infrastruktura jela na
modifikované verzi CentOS, nativní způsob distribuce byly RPM balíky. A tak jsem
vlastně poprvé balil software za peníze.


## Korporátní vsuvka

Nejen díky relativnímu úspěchu Red Hatu i korporátní svět pochopil, že se
svobodného software nedokáže zbavit ani tou nejlepší dezinformační kampaní co
peníze mohou koupit a v roce 2009 došlo k prvnímu příspěvku od Microsoftu do
Linuxového jádra.

V roce 2013 jsem dokončil vysokoškolské studium v Brně, kde nedávno předtím
vyrostla v technologickém parku pobočka Red Hatu, korporace která vsadila na
Linux a otevřený software. Příležitost dále pracovat na svobodném software a u
toho prozkoumat globální fenomén korporace na vlastní kůži mi tehdy dávala smysl
a tak jsem se přidal pod červený klobouk ve snaze zkrotit OpenStack - projekt
otevřeného cloudu v Pythonu.

Původně jsem začal upstream příspěvky, ale eventuelně jsem skončil u distribuce
a automatizace jako obvykle. Jen tentokrát nešlo o pár balíků postavených na
koleně, nýbrž o celou softwarovou distribuci [RDO] čítající desítky různých
projektů.

Postupně jsem s pomocí kolegů automatizoval proces tvorby RPM balíků pro [RDO] a
za tím účelem jsem napsal i několik nástrojů jako `rdopkg`, `rdoinfo`,
`python-distroinfo`, `python-pymod2pkg`, které jsou sice otevřený software, ale
jsou velmi specifické pro konkrétní platformu a tedy nepříliš užitečné mimo své
zamýšlené použití. Některé balíky jsem spravoval jak v RDO tak ve Fedoře/EPEL a
tedy měl jsem možnost si ošahat problematiku distribuce software opravdu
důkladně a na více frontách.


## Hledáme baliče

Můj výzkum všech věcí korporátních byl dokončen v roce 2019, kdy IBM koupilo Red
Hat. Po tom silném zážitku jsem měl v plánu chvíli nepracovat a věnovat se
své vášni jako nezávislý vývojář počítačových her.

Jak se finanční rezervy tenčily, začal jsem pomalu prozkoumávat portály s
nabídkami práce v rámci mentální přípravy na návrat do nekonečného pracovního
procesu. Chtěl jsem si původně dát ještě chvíli pauzu, ale narazil jsem na
pozoruhodnou pozici "Linuxový/á nadšenec/kyně do skriptování a distribucí" pro
CZ.NIC jako na míru pro mě a tak jsem oprášil CV a přihlásil se.

[CZ.NIC] je zájmové sdružení, které má na starosti zejména správu registru
českých domén a provoz domény nejvyšší úrovně .CZ, jak napovídá název (NIC =
Network Information Center). V laboratořích CZ.NIC se pracuje na všemožném
svobodném software souvisejícím s internetem jako je [Knot DNS] (autoritativní
DNS server), [Knot Resolver] (DNS resolver), [BIRD] (Internet Routing Daemon),
či [Datovka] (aplikace pro přístup k datovým schránkam).

Mým úkolem měla být distribuce svobodného software z laboratoří CZ.NIC na
všemožné otevřené systémy - tedy tvorba a správa balíků. Na interview mě
přivítali dva schopní bojovníci za svobodný software, kteří mě ubezpečili o
absenci korporátních nesmyslů jako jsou meetingy či micro-management, a tak jsem
se opět stal profesionálním baličem, ale tentorkát zcela oficiálně a napříč
cílovými platformami.


## Upstream balíky

Začal jsem pracovat na balení projektů [Knot Resolver] a [Knot DNS], které ve
svých repozitářích obsahují zdrojové soubory pro tvorbu balíků na vybrané
distribuce:

* Debian, Ubuntu a další klony (`.deb`)
* Fedora, CentOS, AlmaLinux, Rocky, openSUSE (`.rpm`)
* Arch, Manjaro
* Nix

Za účelem efektivní správy a testování upstream balíků jsem napsal nový nástroj
[apkg], ale to je téma na samostatný
[článek](https://www.root.cz/clanky/apkg-novy-nastroj-pro-automatizaci-upstream-baleni/).



## Distribuční balíky

Ačkoliv mají upstream balíky a repozitáře celou řadu výhod, nejpohodlnější
způsob distribuce software pro uživatele je přímo z repozitářů oblíbené
distribuce.

Začal jsem tedy spravovat balíky i ve všech podporavných verzích Fedory/EPEL, kam
už delší dobu přispívám. Aktualizace Fedora balíku obvykle trvá týden, tedy když
zabalím novou verzi v rámci dnů, uživatelé mají přístup k nejnovějším balíkům do
14 dnů od upstream vydání.

Zdaleka nejžádanější jsou však balíky na Debian/Ubuntu. Vždy jsem chtěl
přispívat do Debianu a konečně jsem k tomu našel i adekvátní příležitost v rámci
práce.


## Debian Maintainer (DM)

Ke konci roku 2020 jsem poprvé
[přispěl](https://tracker.debian.org/news/1181037/accepted-knot-296-1-source-into-unstable/)
do Debianu aktualizací `knot` balíku. Změny jsem připravil v Debian Salsa
(GitLab instance) ale do archivu je nahrál Debian Developer (dále jen **DD**).

Navázel jsem kontakt s DD, který se aktivně angažoval ve správě Knot balíků.
Eventuelně se stal mým mentorem a pomohl mi s přípravou nových verzí a seznámením
s Debian nástroji a infrastrukturou.

Abych mohl samostatně vydávat Debian balíky, začal jsem v březnu 2021
[proces](https://nm.debian.org/process/889/) změny statusu na *Debian Maintainer* (dále jen **DM**),
který může nahrávat do archivu Debianu balíky, ke kterým má oprávnění.

To se záhy zdařilo. Můj GPG klíč byl přidán do DM klíčenky a
konečně jsem v srpnu 2021
[nahrál](https://tracker.debian.org/news/1251246/accepted-knot-311-2-source-amd64-all-into-experimental/)
sám první balík do Debian experimental.

Když je balík akceptován do archivu, automaticky generovaný potvruzjící email začíná slovy:

```
Thank you for your contribution to Debian.
```

Potěšení na mé straně, vážně!

Od té doby udržuji [knot], [knot-resolver] a [bird2] balíky v
nejnovějších verzích v Debian unstable a testing. Ubuntu přejímá změny z
Debianu, takže i tam jsou balíky dříve či později dostupné.


## Přes útrapy ke hvězdám

V průběhu času jsem vydal mnoho nových verzí Debian balíků a naučil se lépe
pracovat s dostupnými nástroji a systémy. Nicméně jsem občas narazil na
problémy, které jsem nemohl jakožto DM samostaně vyřešit.
Například při změně API v [Knot DNS] je třeba zvýšit číslo v názvu podbalíku
(`libknot12` -> `libknot13`), což systém bere jako nahrání nového balíku
(NEW) a na to DM oprávnění nestačí. Občas také potřebuji
aktualizovat balíky závislostí (jako např Lua knihovny použité v
[Knot Resolver]), ke kterým nemám oprávnění.

Standardní doba odpovědí v Debianu se počítá v týdnech a tak pro mě i triviální
problém často znamenal dlouhé zbytečné čekání na intervenci DD. Po jedné takové
intervenci mi můj moudrý mentor navrhnul, abych se sám stal DD. Nebyl jsem si
jistý, jestli na to už mám dostatek zkušeností, ale nechal jsem si poradit.

Po necelých 2 letech přispívání do Debianu jsem v září 2022 začal
[proces](https://nm.debian.org/process/1115/) změny statusu na
*Debian Developer, uploading*.


## Přísný Proces

Dle mého názoru jsou procesy Debianu skvěle navržené a proces změny
statusu na DD toho byl zářným příkladem. Mimo jiné jsem v sérii 30 mailů
prokázal jednak svou technickou dovednost a své znalosti Debianu, ale také
filosofické a právní pochopení problematiky svobodného software. Vysvětloval
jsem třeba rozdíl mezi "free as in beer" a "free as in speech", studoval
archaické licence a posuzoval zda jsou svobodné podle _Debian Free Software
Guidelines_ ([DFSG]), nebo vysvětloval jak funguje Debian volební systém.

Účastnil jsem se za život mnoha testů, zkoušek, pohovorů - vstupních i
výstupních, od akademie po korporát. Žádný z nich mi nepřišel ani zdaleka tak
důkladný, efektivní, všestranný a celkově adekvátní jako vsuptní brána do Debianu.
DD status nelze koupit za peníze ani získat na počkání.

Jsou to efektivní procesy díky kterým Debian úspěšně existuje už 30 let navzdory
měnícímu se světu. Lidé odchází a přichází, ale projekt pokračuje dál na základě
myšlenek jasně popsaných v jeho ustavujících dokumentech.

Obdobně přísné procesy ohledně stability balíků pro Debian stable z něj dělají
jeden z nejstabilnějších OS dneška. Ne náhodou je Debian standardní obraz v
mnoha CI systémech včetně GitHubu. Svůj server aktualizuji od Debian 7 Wheezy a
po dekádě normálně funguje.


## Debian Developer (DD)

Po půl roce proces úspěšně doběhl a v březnu 2023 jsem se stal _Debian Developer,
uploading_.

Prošel jsem dister kraj, ale nakonec jsem skončil tam, kde jsem před 20 lety
začal svou pouť - v Debianu, pevnosti svobodného software.

Vždy jsem obdivoval neopěvované hrdiny v pozadí distribucí, kteří svádí nevděčný
boj se software za nás všechny. Je mi velkou ctí i radostí se stát jedním z nich
a budu usilovně pracovat na dodání balíků v kvalitě a stabilitě, která se od
Debianu čeká.

Za odměnu, že jste dočetli až sem, mé Debian tetování:

![Debian tetování](https://gitlab.nic.cz/packaging/packaging-docs/-/raw/master/img/debian_tattoo.png?inline=false "Debian tetování")

Rozloučím se virtuálním přípitkem na vítezství svobodného software nad
korporátní hydrou 🍻


[RDO]: https://www.rdoproject.org/
[CZ.NIC]: https://www.nic.cz/

[Knot DNS]: https://www.knot-dns.cz/
[Knot Resolver]: https://www.knot-resolver.cz/
[BIRD]: https://bird.network.cz/
[Datovka]: https://www.datovka.cz/

[apkg]: https://gitlab.nic.cz/packaging/apkg

[knot]: https://tracker.debian.org/pkg/knot
[knot-resolver]: https://tracker.debian.org/pkg/knot-resolver
[bird2]: https://tracker.debian.org/pkg/bird2

[DFSG]: https://www.debian.org/social_contract#guidelines
