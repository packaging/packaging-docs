# rust packaging adventures

## Debian

### status

Rust versions seem to steadily and slowly flow into Debian as usual.

There is the [Debian Rust Packaging
Policy](https://wiki.debian.org/Teams/RustPackaging/Policy) and the [Debian
Rust Packaging Team](https://wiki.debian.org/Teams/RustPackaging) which seems
to be active and prefers IRC: `#debian-rust` on `irc.oftc.net`

Rust packaging is supported and integrated into Debian using dedicated tools like `dh-cargo`
and `debcargo` which seem to be active.

[debcargo-conf](https://salsa.debian.org/rust-team/debcargo-conf) repo holds
metadata and scripts to build many rust apps and libs from crates - it might
be easiest way to package including missing deps.


### experiments on Debian unstable

I've succesfully built [knot-resolver-manager] locally using `cargo build` which
pulls in all deps during build and tested resulting binary to start.

I've succesfully built `ripgrep` rust package on my Debian unstable VM.

When building custom [knot-resolver-manager] package based on Debian
`ripgrep`, I discovered many Rust deps are avaliable from Debian repos as
`librust-$PROJECT-dev` but I got stuck on missing `dbus-tokio` which isn't
available from debian repos:

```
error: no matching package named `dbus-tokio` found
location searched: registry `https://github.com/rust-lang/crates.io-index`
required by package `knot-resolver-manager v0.1.0
```

The error is weird given `dbus-tokio` is available from `crates.io` but
I guess it simply means `dbus-tokio` isn't available during build.


## Fedora

### status

Fedora 33 and 32 have the latest Rust.

Fedora [Rust SIG](https://fedoraproject.org/wiki/SIGs/Rust) has
[several](https://developer.fedoraproject.org/tech/languages/rust/rust-sig.html)
pages [online](https://docs.pagure.org/fedora-rust.sig/index.html) but it
doesn't seem to be very active judging from various last edit dates
found on these pages as well as [rust Fedora mailing
list](https://lists.fedoraproject.org/archives/list/rust@lists.fedoraproject.org/)
which received about 4 emails during 2020 and few tens of emails during its
entire existence.

[rust2rpm](https://pagure.io/fedora-rust/rust2rpm/) tool to create packages
from rust crates received commits during 2020.


### experiments on Fedora 33

Fedora `rust-ripgrep` package is
[broken](https://koschei.fedoraproject.org/package/rust-ripgrep) on failing
tests. At least it seems to compile before that.

I've succesfully built [knot-resolver-manager] locally using `cargo build` and tested resulting binary to start.



[knot-resolver-manager]: https://gitlab.nic.cz/knot/knot-resolver-manager
