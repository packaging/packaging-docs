#!/usr/bin/bash
# this script can be used to convert from Markdown
# to HTML insertable into WordPress blog @ blog.nic.cz

INPUT=$1
if [[ -z "$INPUT" ]]; then
    echo "usage: $0 INPUT_FILE.md"
    exit 1
fi
NAME=$(basename $INPUT)
OUTDIR=blog
OUTBASE=$OUTDIR/${NAME%.md}

OUT_PANDOC=$OUTBASE.pandoc.html
OUT_SED=$OUTBASE.sed.html
OUT_FINAL=$OUTBASE.html

echo "$INPUT -> $OUT_FINAL"
mkdir -p "$OUTDIR"

pandoc "$INPUT" -t html -o "$OUT_PANDOC"
cp "$OUT_PANDOC" "$OUT_SED"

# remove header IDs
sed -e 's# id="[^"]*"##g' -i "$OUT_SED"
# replace <code> blocks with <tt> (borked CSS on blog.nic.cz)
sed -e 's#<code>#<tt>#g' -e 's#</code>#</tt>#g' -i "$OUT_SED"

# remove line wraps so that blog software doesn't insert unwanted newlines on paste
tidy -wrap 0 --show-body-only true -output "$OUT_FINAL" "$OUT_SED" 

echo -e "\\n$OUT_FINAL"
